<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            ['name'=>'Site Title','slug'=>'site-title','value'=>'test'],
            ['name'=>'Site Logo','slug'=>'site-logo','value'=>''],
            ['name'=>'Site Favicon','slug'=>'site-favicon','value'=>''],
            ['name'=>'Meta Keyword','slug'=>'site-keyword','value'=>'test'],
            ['name'=>'Meta Description','slug'=>'site-description','value'=>'test'],
            ['name'=>'Site Name','slug'=>'site-name','value'=>'Site Name'],
            ['name'=>'Email','slug'=>'email','value'=>'test@test.com'],
            ['name'=>'About Us','slug'=>'aboutUs','value'=>'This is test For about us'],
            ['name'=>'Footer Text','slug'=>'footer-text','value'=>'2016 All Rights Reserved • www.abcd.com '],
            ['name'=>'DMCA','slug'=>'dmca','value'=>'DMCA dmcs'],
        ];

        foreach ($settings as $key => $value) {
            DB::table('settings')->insert($value);
        }
    }
}
