@extends($theme)

@section('content')
<div style="margin-bottom:5px;" class="row">
<div class="col-md-3">
    <div class="sm-st clearfix">
        <span class="sm-st-icon st-violet"><i class="fa fa-sitemap"></i></span>
        <div class="sm-st-info">
            <span>{{ $totalCategory }}</span>
            Total Categories
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="sm-st clearfix">
        <span class="sm-st-icon st-blue"><i class="fa fa-desktop"></i></span>
        <div class="sm-st-info">
            <span>{{ $totalPost }}</span>
            Total Posts
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="sm-st clearfix">
        <span class="sm-st-icon st-green"><i class="fa fa-star"></i></span>
        <div class="sm-st-info">
            <span>{{ $totalDomain }}</span>
            Total Domains
        </div>
    </div>
</div>
</div>
@endsection