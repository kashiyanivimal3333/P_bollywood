@extends($login)
@section('content')
  <div class="container">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            {!! Form::open(array('route' => 'admin.login','class'=>'form-signin')) !!}
                <span id="reauth-email" class="reauth-email"></span>
                <label class="panel-login">
                        <div class="login_result">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" style="width:100%">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </label>
                {!! Form::text('email', Input::get('email'), array('placeholder' => 'Email','class' => 'form-control','id'=>'inputEmail')) !!}
                {!! Form::password('password', array('placeholder' => 'Password','class'=>'form-control','id'=>'inputPassword')) !!}
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
            {!! Form::close() !!}
            <!-- /form -->
        </div><!-- /card-container -->
    </div><!-- /container -->
@endsection