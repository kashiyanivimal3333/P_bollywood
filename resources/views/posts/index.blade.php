@extends($theme)

@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@stop

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Manage {{ ucwords($moduleTitleP) }}
            </h1>
        </div>
        <div class="pull-right">
                <button class="btn btn-success" data-toggle="modal" data-target="#create-{{ $moduleTitleS }}"><i aria-hidden="true" class="fa fa-plus"></i> Create New {{ ucwords($moduleTitleS) }}</button>
                <button class="btn btn-info search-modules"><i aria-hidden="true" class="fa fa-search"></i> Search</button>
                @include($moduleTitleP.'.create')
        </div>
    </div>
</div>

@include($moduleTitleP.'.search')

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th width="50px">No</th>
            <th>Category</th>
            <th>Title</th>
            <th>Time</th>
            <th>Image</th>
            <!-- <th>Meta Keyword</th> -->
            <!-- <th>Meta Description</th> -->
            <th width="200px">Action</th>
        </tr>
    </thead>
    <tbody>
        @include($moduleTitleP.'.data')
    </tbody>
</table>

@if(!empty($data) && $data->count())
{!! $data->appends(Input::all())->render() !!}
@endif

<script type="text/javascript">
    $(".category-select").select2();
</script>

@endsection