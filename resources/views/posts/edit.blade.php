<!-- Modal -->
<div class="modal fade" id="{{ $moduleTitleS }}-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content">
            {!! Form::model($value, ['method' => 'PATCH','route' => ["admin.$moduleTitleP.update", $value->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit {{ ucwords($moduleTitleS) }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <?php 
                                $postCategoryArray = explode(',', $value->post_categories_list) 
                            ?>
                            <div class="form-group">
                                <strong>Category:</strong>
                                {!! Form::select('category_id[]', $subCategoryList, $postCategoryArray, array('class'=>'form-control category-select','multiple'=>'multiple','style'=>'width:100%;')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Title:</strong>
                                {!! Form::text('title', Input::get('title'), array('placeholder' => 'title','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Select Domain:</strong>
                                {!! Form::select('domain_id', [''=>'select domain']+$domainList, null,array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Video Code:</strong>
                                {!! Form::text('video_code', Input::get('video_code'), array('placeholder' => 'video code','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Times:</strong>
                                {!! Form::text('time',null,array('class' => 'form-control')); !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Image:</strong>
                                {!! Form::file('image', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Description:</strong>
                                {!! Form::textarea('description', Input::get('description'),array('placeholder' => 'description','class'=>'form-control textarea-style')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Meta Keyword:</strong>
                                {!! Form::textarea('meta_keyword', Input::get('meta_keyword'),array('placeholder' => 'meta_keyword','class'=>'form-control textarea-style')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Meta Description:</strong>
                                {!! Form::textarea('meta_description', Input::get('meta_description'),array('placeholder' => 'meta description','class'=>'form-control textarea-style')) !!}
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>