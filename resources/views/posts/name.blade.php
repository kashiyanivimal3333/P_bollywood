<!-- Modal -->
<div class="modal fade" id="{{ $moduleTitleS }}-names-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content">
            {!! Form::open(array('route' => 'admin.namesMeta.store','method'=>'POST','autocomplete'=>'off')) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Names Meta</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    {!! Form::hidden('post_id', $value->id) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="form-group">
                                <strong>Name Title:</strong>
                                {!! Form::textarea('name_title', Input::get('name_title',$value->name_title),array('placeholder' => 'name title','class'=>'form-control textarea-style')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="form-group">
                                <strong>Name Keyword:</strong>
                                {!! Form::textarea('name_keyword', Input::get('name_keyword',$value->name_keyword),array('placeholder' => 'name keyword','class'=>'form-control textarea-style')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="form-group">
                                    <strong>Name Description:</strong>
                                {!! Form::textarea('name_description', Input::get('name_description',$value->name_description),array('placeholder' => 'name description','class'=>'form-control textarea-style')) !!}
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
</div>