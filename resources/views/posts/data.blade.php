@if(!empty($data) && $data->count())
    @foreach($data as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $value->post_categories_name }}</td>
            <td>{{ $value->title }}</td>
            <td>{{ $value->time }}</td>
            <td align="center"><img src="/public/upload/posts/{{ $value->image }}" class="post-img"></td>
            <td>
                <button class="padding-bt btn btn-primary" data-toggle="modal" data-target="#{{ $moduleTitleS }}-{{ $value->id }}"><i aria-hidden="true" class="fa fa-edit"></i> Edit</button>
                <button class="padding-bt btn btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ route('admin.'.$moduleTitleP.'.destroy',$value->id) }}"><i aria-hidden="true" class="fa fa-trash-o"></i> Delete</button>
                <br /><br />
                <button class="padding-bt btn btn-primary" data-toggle="modal" data-target="#{{ $moduleTitleS }}-tags-{{ $value->id }}"> Manage Post Tags</button>
                <br /><br />
                <button class="padding-bt btn btn-primary" data-toggle="modal" data-target="#{{ $moduleTitleS }}-names-{{ $value->id }}"> Name</button>
                @include($moduleTitleP.'.edit')
                @include($moduleTitleP.'.tags')
                @include($moduleTitleP.'.name')
            </td>
        </tr>
    @endforeach
@endif