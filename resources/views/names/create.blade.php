<!-- Modal -->
<div class="modal fade" id="create-{{ $moduleTitleS }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create {{ ucwords($moduleTitleS) }}</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'admin.'.$moduleTitleP.'.store','method'=>'POST','autocomplete'=>'off')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-lable">Name</label>
                                    {!! Form::text('name', Input::get('name'), array('placeholder' => 'Name','class' => 'form-control user-name-edit')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <br/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud create-crud">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>