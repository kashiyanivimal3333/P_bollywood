@extends($frontThemeNew)

@section('pageTitle')
	<title>{{ $titleName }}</title>
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $titleName }}</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<p>{{ $description }}</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<p><strong>By</strong> : Watch Hot Actress Videos</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="post-detail-img">
			<a href="{{ $postData->domain }}result?c={{ $postData->video_code }}" target="_blank">
				<img src="{{ setPostImageOriginal($postData->image) }}">
			<i class="glyphicon glyphicon-play-circle"></i>
			</a>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 watch-video-btn">

		<p>Online Watch Dailymotion & YouTube 1080p High Quality Bollywood Videos</p>

	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<span><strong>Category</strong>:
			@if(!empty($postData->post_categories_name_display) && !empty($postData->post_categories_slug))
				<?php
					$nameDisplayArray = explode(',', $postData->post_categories_name_display);
					$categorySlugArray = explode(',', $postData->post_categories_slug);
				?>
				@foreach($nameDisplayArray as $ckey=>$cvalue)
					@if($ckey != 0)
					,
					@endif
					<a href="{{ route('front.category',$categorySlugArray[$ckey]) }}">{{ $cvalue }}</a>
				@endforeach
			@endif
		</span>
	</div>
	<div class="col-md-6 post-detail-uploaded-on" align="right">
		<span><strong>Uploaded on</strong>: {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $postData->created_at)->format('d-m-Y') }}</span>
	</div>
</div>
<br />
<div class="row">
    <div class="col-md-12" align="center">
        <strong> Share It On </strong>
        <br /><br />
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
		    <a class="a2a_button_facebook a2a_counter"></a>
		    <a class="a2a_button_pinterest a2a_counter"></a>
		    <a class="a2a_button_linkedin a2a_counter"></a>
		    <a class="a2a_button_tumblr a2a_counter"></a>
		    <a class="a2a_button_reddit a2a_counter"></a>
		    <a class="a2a_dd a2a_counter" href="https://www.addtoany.com/share"></a>
		</div>

		<script async src="https://static.addtoany.com/menu/page.js"></script>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Related Videos</h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        @if(!empty($relatedPostData) && $relatedPostData->count())
        	<ul class="list-unstyled video-list-thumbs row">
				@foreach($relatedPostData as $key=>$value)
					@include('front.include.postBox')
				@endforeach
			</ul>
		@endif
    </div>
</div>

@stop