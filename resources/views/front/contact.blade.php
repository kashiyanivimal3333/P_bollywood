@extends($frontThemeNew)

@section('pageTitle')
	<title>Contact</title>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Contact Us</h1>
    </div>
</div>
<!-- /.row -->

<!-- Projects Row -->
<div class="row">
	<div class="contact-form" style="padding: 30px 0px;">
		@if($message = Session::get('success'))
		<div class="alert alert-success">
            <strong>{{ $message }}</strong>
        </div>
		@endif
        
		@if(count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

  	    {!! Form::open(array('route' => 'front.feedback.create', 'method'=>'post', 'autocomplete'=>'off')) !!}
  	    	<div class="row">
	  	    	<div class="col-md-9">
				    <div class="form-group">
					    <label>Name</label>
					    {!! Form::text('name',Input::get('name'),array('placeholder'=>'First &amp; Last Name','class'=>'form-control','required'=>'required')) !!}
					</div>
	  	    	</div>
  	    	</div>

  	    	<div class="row">
	  	    	<div class="col-md-9">
					<div class="form-group">
					    <label>E-mail</label>
					    {!! Form::email('email',Input::get('email'),array('placeholder'=>'example@xyz.com','class'=>'form-control','required'=>'required')) !!}
					</div>
	  	    	</div>
	  	    </div>

	  	    <div class="row">
	  	    	<div class="col-md-9">
				    <div class="form-group">
					    <label>Subject</label>
					    {!! Form::textarea('message',Input::get('message'),array('placeholder'=>'Enter Message','class'=>'form-control','required'=>'required', 'style'=>'max-height:100px;')) !!}
					</div>
	  	    	</div>
	  	    </div>

		   	<div>
		   		<button type="submit" class="btn btn-success">Submit</button>
		  	</div>
	    {!! Form::close() !!}
    </div>
</div>
<!-- /.row -->

@stop