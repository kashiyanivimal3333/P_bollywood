@extends($frontTheme)

@section('pageTitle')
	<title>
		@if(Input::get('page') >= 2)
			page - {{ Input::get('page') }} -
		@endif
		{{ $pageTitle }}
	</title>
@stop

@section('content')

<style type="text/css">
	.desc {
		margin: 15px 0px;
	}
	.desc strong{
		font-family: "Ropa Sans",sans-serif;
		font-weight: bold;
	}
	.desc p{
		font-family: "Ropa Sans",sans-serif;
		text-align: justify;
	}
</style>

<div class="content">
	<div class="left-content">
		<div class="searchbar">
			<div class="search-left">
				<h1>Tag : {{ $tagData->name }}</h1>
			</div>
			<div class="clear"> </div>
		</div>

		<div class="desc">
			<strong>Description:</strong>
			<p>{{$tagData->meta_description}}</p>
		</div>
		<div class="box">
			<div class="grids">
				@if(!empty($postData) && $postData->count())
					@foreach($postData as $key=>$value)
						@include('front.include.postBox',$value)
					@endforeach
				@endif
			</div>
			<div class="clear"> </div>

			@if(!empty($postData) && $postData->count())
				{!! $postData->appends(Input::all())->render() !!}
			@endif
		
		</div>
		<div class="clear"> </div>
	</div>
	@include('frontTheme.rightSidebar')
	<div class="clear"> </div>
</div>

@stop