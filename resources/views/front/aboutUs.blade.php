@extends($frontThemeNew)

@section('pageTitle')
	<title>About Us</title>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">About Us</h1>
    </div>
</div>
<!-- /.row -->

<!-- Projects Row -->
<div class="row">
	<p>{{ $settingsData['aboutUs'] }}</p>
</div>
<!-- /.row -->

@stop