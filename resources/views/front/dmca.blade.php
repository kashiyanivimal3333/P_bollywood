@extends($frontTheme)

@section('pageTitle')
	<title>DMCA</title>
@stop

@section('content')

<div class="content">
	<div class="left-content">
		<div class="searchbar">
			<div class="search-left">
				<p>DMCA</p>
			</div>
			<div class="clear"> </div>
		</div>
		<br />
		<p>{{ $settingsData['dmca'] }}</p>
		<div class="clear"> </div>
	</div>
@include('frontTheme.rightSidebar')
<div class="clear"> </div>
</div>

@stop