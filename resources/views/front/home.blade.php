@extends($frontThemeNew)

@section('pageTitle')
	<title>{{ $pageTitle }}</title>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Latest
            <small>Videos</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<!-- Projects Row -->
<div class="row">
	@if(!empty($postData) && $postData->count())
		<ul class="list-unstyled video-list-thumbs row">
			@foreach($postData as $key=>$value)
	    		@include('front.include.postBox')
			@endforeach
		</ul>
	@endif
</div>
<!-- /.row -->

<hr>

<!-- Pagination -->
@if(!empty($postData) && $postData->count())
	{!! $postData->appends(Input::all())->render() !!}
@endif
<!-- /.row -->

<hr>

@endsection