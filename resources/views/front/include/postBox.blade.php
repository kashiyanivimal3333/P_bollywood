<li class="col-lg-4 col-sm-4 col-xs-6">
	<a href="{{ route('front.post.detail',$value->slug) }}" title="{{ $value->title }}">
		<img src="{{ setPostTHImage1($value->image,257,161) }}" alt="Barca" class="img-responsive" height="140px" />
		<h2>{{ str_limit($value->title,70) }}</h2>
		<span class="glyphicon glyphicon-play-circle"></span>
		<span class="duration">{{ $value->time }}</span>
	</a>
</li>