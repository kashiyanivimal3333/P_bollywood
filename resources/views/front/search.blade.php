@extends($frontTheme)

@section('pageTitle')
	<title>{{ $settingsData['site-title'] }}</title>
@stop

@section('content')

<div class="content">
	<div class="left-content">
		<div class="searchbar">
			<div class="search-left">
				<p>Your Search Result "{{ Input::get('text') }}" </p>
			</div>
			<div class="search-right">
				<form action="{{ route('front.search') }}" method="GET">
					<input type="text" value="{{ Input::get('text') }}" name="text">
					<input type="submit" value="" />
				</form>
			</div>
			<div class="clear"> </div>
		</div>
		<div class="box">
		<div class="grids">
			@if(!empty($postData) && $postData->count())
				@foreach($postData as $key=>$value)
					@include('front.include.postBox',$value)
				@endforeach
			@else
				<p>Result Not Found</p>
			@endif
		</div>
		<div class="clear"> </div>
		
	</div>
	<div class="clear"> </div>
</div>
@include('frontTheme.rightSidebar')
<div class="clear"> </div>
</div>

@stop