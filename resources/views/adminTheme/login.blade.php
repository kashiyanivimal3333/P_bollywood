<!DOCTYPE html>
<html lang="en">
<head>
<title>{{ $projectTitle }}</title>
<!-- Bootstrap Core CSS -->
<link href="{{ asset('/public/adminTheme/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('/public/adminTheme/css/login.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
</body>

<!-- jQuery -->
<script src="{{ asset('/public/adminTheme/js/jquery.js') }}"></script>
<script src="{{ asset('/public/adminTheme/js/TweenLite.min.js') }}"></script>
<script src="{{ asset('/public/adminTheme/js/login.js') }}"></script>
</html>