<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $projectTitle }} | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <link rel="stylesheet" type="text/css" href="{{ asset('/adminTheme/css/select2.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/adminTheme/js/select2.min.js') }}">
        <!-- bootstrap 3.0.2 -->
        <link href="{{ asset('/public/adminTheme/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- font Awesome -->
        <link href="{{ asset('/public/adminTheme/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- Ionicons -->
        <link href="{{ asset('/public/adminTheme/css/ionicons.min.css') }}" rel="stylesheet">
        <!-- iCheck for checkboxes and radio inputs -->
        <link href="{{ asset('/public/adminTheme/css/iCheck/all.css') }}" rel="stylesheet">
        <link href="{{ asset('/public/adminTheme/css/datepicker/datepicker3.css') }}" rel="stylesheet">
        <link href="{{ asset('/public/adminTheme/css/custom.css') }}" rel="stylesheet">
        <!-- bootstrap wysihtml5 - text editor -->
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="{{ asset('/public/adminTheme/css/style.css') }}" rel="stylesheet">

        <!-- jQuery 2.0.2 -->
        <script src="{{ asset('/public/adminTheme/js/jquery.js') }}"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        
        <style type="text/css">
            #process_img{
                opacity: 0.7;
                position: fixed;
                text-align: center;
                top: 0;
                width: 80%;
                z-index: 1001;
                display: none;
                background: white;
                height: 100%;
            }
            #process_img img{
                position: relative;
                top: 40%;
            }
        </style>

        <script type="text/javascript">
            var current_page_url = "<?php echo URL::current(); ?>";
            var current_page_fullurl = "<?php echo URL::full(); ?>";
        </script>

        @yield('style')
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        @include('adminTheme.header')

        <div class="wrapper row-offcanvas row-offcanvas-left">
            @include('adminTheme.sidebar')
            <aside class="right-side">
                <section class="content">
                    @include('adminTheme.alert')
                    @yield('content')
                </section>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <div id="process_img" style="display: none;">
            <img src="/public/adminTheme/img/loading.gif"/>
        </div>

        <!-- jQuery UI 1.10.3 -->
        <script src="{{ asset('/public/adminTheme/js/jquery-ui-1.10.3.min.js') }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('/public/adminTheme/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/public/adminTheme/js/bootbox.js') }}"></script>
        <script src="{{ asset('/public/adminTheme/js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('/public/adminTheme/js/Director/app.js') }}"></script>
        <script src="{{ asset('/public/adminTheme/js/jquery.form.min.js') }}"></script>
        <!-- Director dashboard demo (This is only for demo purposes) -->
        <script src="{{ asset('/public/adminTheme/js/crud.js') }}"></script>
        <script src="{{ asset('/public/adminTheme/js/custom.js') }}"></script>
        @yield('script')

    </body>
</html>