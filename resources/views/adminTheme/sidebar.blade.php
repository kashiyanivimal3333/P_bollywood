<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/public/adminTheme/img/avatar5.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, {{ auth()->guard('admin')->user()->fullname }}</p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php 
            $currentPageURL = URL::current(); 
            $pageArray = explode('/', $currentPageURL);
            $pageActive = isset($pageArray[4]) ? $pageArray[4] : 'test';
        ?>
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'home' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.home') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
        </ul>
        <!-- <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'users' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.users.index') }}">
                <i class="fa fa-users"></i> <span>Users</span>
                </a>
            </li>
        </ul> -->
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'admins' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.admins.index') }}">
                <i class="fa fa-user"></i> <span>Admins</span>
                </a>
            </li>
        </ul>
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'categories' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.categories.index') }}">
                <i class="fa fa-sitemap"></i> <span>Categories</span>
                </a>
            </li>
        </ul>
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'sub-categories' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.sub-categories.index') }}">
                <i class="fa fa-sitemap"></i> <span>Sub Categories</span>
                </a>
            </li>
        </ul>
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'posts' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.posts.index') }}">
                <i class="fa fa-desktop"></i> <span>Posts</span>
                </a>
            </li>
        </ul>
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'tags' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.tags.index') }}">
                <i class="fa fa-tags"></i> <span>Tags</span>
                </a>
            </li>
        </ul>
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'names' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.names.index') }}">
                <i class="fa fa-tags"></i> <span>Names</span>
                </a>
            </li>
        </ul>
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'domains' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.domains.index') }}">
                <i class="fa fa-star"></i> <span>Domains</span>
                </a>
            </li>
        </ul>
        <ul class="sidebar-menu">
            <li class="{{ $pageActive == 'feedback' ? 'active' : ''  }}">
                <a href="{{ URL::route('admin.feedback') }}">
                <i class="fa fa-fw fa-comment"></i> <span>Feedback</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>