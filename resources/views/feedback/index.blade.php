@extends($theme)

@section('pageTitle')
<title>Feedback</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Feedback Management
            </h1>
        </div>
    </div>
</div>

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Email</th>
            <th>Message</th>
            <th>Date & Time</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($displayData) && $displayData->count())
            <?php $no = 1;?>
            @foreach($displayData as $key => $value)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ $value->message }}</td>
                    <td>{{ $value->created_at }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>


@if(!empty($displayData) && $displayData->count())
{!! $displayData->appends(Input::all())->render() !!}
@endif


<script type="text/javascript">
    $(window).scroll(function(){
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){
           // $.loadPostData();
        }
    });
</script>

@endsection