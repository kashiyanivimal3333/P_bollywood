<!-- Modal -->
<div class="modal fade" id="create-{{ $moduleTitleS }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create {{ ucwords($moduleTitleS) }}</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'admin.'.$moduleTitleP.'.store','method'=>'POST','autocomplete'=>'off')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Display Name:</strong>
                                {!! Form::text('name_display', Input::get('name_display'), array('placeholder' => 'display name','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Name:</strong>
                                {!! Form::text('name', Input::get('name'), array('placeholder' => 'name','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Meta Keyword:</strong>
                                {!! Form::textarea('meta_keyword', Input::get('meta_keyword'),array('placeholder' => 'meta_keyword','class'=>'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Meta Description:</strong>
                                {!! Form::textarea('meta_description', Input::get('meta_description'),array('placeholder' => 'meta description','class'=>'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud create-crud">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>