<!-- Modal -->
<div class="modal fade" id="{{ $moduleTitleS }}-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content">
            {!! Form::model($value, [
    'method' => 'PATCH',
    'route' => ["admin.$moduleTitleP.update", $value->id]
]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit {{ ucwords($moduleTitleS) }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Fullname:</strong>
                                {!! Form::text('fullname', Input::get('fullname',$value->fullname), array('placeholder' => 'Name','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Email:</strong>
                                {!! Form::text('email', Input::get('email',$value->email), array('placeholder' => 'Email','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Password:</strong>
                                {!! Form::password('password', array('placeholder' => 'Password','class'=>'form-control edit-password','AutoComplete'=>'off')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Confirm Password:</strong>
                                {!! Form::password('password_confirmation', array('placeholder' => 'Confirm Password','class'=>'form-control edit-password-c')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Age:</strong>
                                {!! Form::text('age', $value->age,array('placeholder' => 'Age','class'=>'form-control edit-password','AutoComplete'=>'off')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <strong>Sex:</strong>
                            <div class="form-group">
                                <label>{!! Form::radio('sex', '0', $value->sex == 0 ? true : false, array('class' => '')) !!} Male</label>                                
                                <label>{!! Form::radio('sex', '1', $value->sex == 1 ? true : false, array('class' => '')) !!} Female</label>                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Image:</strong>
                                {!! Form::file('imagepath', array('class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>