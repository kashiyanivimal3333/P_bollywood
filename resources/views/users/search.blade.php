<div class="panel panel-default filter-panel {{Input::get("filter",false)?'filter-in':'filter-out'}}">
    <div class="panel-heading">
        <h3 class="panel-title"><strong>Searching</strong></h3>
    </div>
    <div class="panel-body">
        {!! Form::open(array('method'=>'get','class'=>'form-filter form-inline')) !!}
        <ul class="search">
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[fullname][value]', 'Fullname',array('class'=>'control-label')) !!}
                    {!! Form::text('filter[fullname][value]',Input::get('filter.fullname.value'),array('class'=>'form-control')) !!}{!! Form::hidden('filter[fullname][type]','7') !!}
                </div>
            </li>
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[email][value]', 'Email',array('class'=>'control-label')) !!}
                    {!! Form::text('filter[email][value]',Input::get('filter.email.value'),array('class'=>'form-control')) !!}{!! Form::hidden('filter[email][type]','7') !!}
                </div>
            </li>
            <li>
                <div class='form-group'>
                    <br/>
                    <button class="btn btn-success" style="margin-top:5px;">Submit</button>
                </div>
            </li>
        </ul>
        {!! Form::close() !!}
    </div>
</div>
