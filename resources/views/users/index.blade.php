@extends($theme)

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Manage {{ ucwords($moduleTitleP) }}
            </h1>
        </div>
        <div class="pull-right">
                <button class="btn btn-success" data-toggle="modal" data-target="#create-{{ $moduleTitleS }}"><i aria-hidden="true" class="fa fa-plus"></i> Create New {{ ucwords($moduleTitleS) }}</button>
                <button class="btn btn-info search-modules"><i aria-hidden="true" class="fa fa-search"></i> Search</button>
                @include($moduleTitleP.'.create')
        </div>
    </div>
</div>

@include($moduleTitleP.'.search')

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th width="50px">No</th>
            <th>Image</th>
            <th>Fullname</th>
            <th>Email</th>
            <th>Ban</th>
            <th>Created At</th>
            <th width="260px">Action</th>
        </tr>
    </thead>
    <tbody>
        @include($moduleTitleP.'.data')
    </tbody>
</table>

@if(!empty($data) && $data->count())
{!! $data->appends(Input::all())->render() !!}
@endif

@endsection