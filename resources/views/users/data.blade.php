@if(!empty($data) && $data->count())
    @foreach($data as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>
                <img src="{{ getUserImagePath($value->imagepath) }}" style="width:50px;">
            </td>
            <td>{{ $value->fullname }}</td>
            <td>{{ $value->email }}</td>
            <td>
                @if($value->ban == 0)
                <label class="label label-success">No</label>
                @else
                <label class="label label-danger">Yes</label>
                @endif
            </td>
            <td>{{ $value->created_at}}</td>
            <td>
                @if($value->ban == 1)
                    <a href="{{ route('admin.users.revokeuser',$value->id) }}" class="padding-bt btn btn-warning"><i aria-hidden="true" class="fa fa-circle-o"></i> Revoke</a>
                @else
                    <a class="btn btn-success ban padding-bt" data-id="{{ $value->id }}" data-action="{{ URL::route('admin.users.ban') }}"><i aria-hidden="true" class="fa fa-ban"></i> Ban</a>
                @endif
                <button class="padding-bt btn btn-primary" data-toggle="modal" data-target="#{{ $moduleTitleS }}-{{ $value->id }}"><i aria-hidden="true" class="fa fa-edit"></i> Edit</button>
                @include($moduleTitleP.'.edit')
                <button class="padding-bt btn btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ route('admin.'.$moduleTitleP.'.destroy',$value->id) }}"><i aria-hidden="true" class="fa fa-trash-o"></i> Delete</button>
            </td>
        </tr>
    @endforeach
@endif