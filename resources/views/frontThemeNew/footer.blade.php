<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>{{ date('Y') }} {{ $settingsData['footer-text'] }}</p>
        </div>
    </div>
    <!-- /.row -->
</footer>