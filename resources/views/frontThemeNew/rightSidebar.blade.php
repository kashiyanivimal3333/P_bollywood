<h3 class="cat-title">Categories</h3>
<ul class="list-group">
	@if(!empty($categoryListRight))
		@foreach($categoryListRight as $key=>$value)
			<li class="list-group-item list-group-headding" style="margin-top:10px;">
				<h4>{{ $value['data']['name'] }}</h4>
					@if(isset($value['subCategory']))		
						<ul>
							<?php $a=0; ?>
							@foreach($value['subCategory'] as $skey=>$svalue)
								@if($a < 4)
									<li><a href="{{ route('front.category',$svalue['subCategoriesSlug']) }}">{{ $svalue['subCategoriesName'] }}</a></li>
									@else
									<li style="display:none;" class="subcat-show-hide-{{ $svalue->id }}"><a href="{{ route('front.category',$svalue['subCategoriesSlug']) }}">{{ $svalue['subCategoriesName'] }}</a></li>
								@endif
							<?php $a++; ?>
							@endforeach
							<div class="subcathandel">
								<span class="subCategory-show-hide" data-id="{{ $svalue->id }}">Show More....</span>
							</div>
						</ul>
					@endif
			</li>
		@endforeach
	@endif
</ul>
