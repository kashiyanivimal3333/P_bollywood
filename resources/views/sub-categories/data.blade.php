@if(!empty($data) && $data->count())
    @foreach($data as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $value->categoryName }}</td>
            <td>{{ $value->name_display }}</td>
            <td>{{ $value->name }}</td>
            <td style="word-break: break-all;">{{ $value->meta_keyword }}</td>
            <td style="word-break: break-all;">{{ $value->meta_description }}</td>
            <td>
                <button class="padding-bt btn btn-primary" data-toggle="modal" data-target="#{{ $moduleTitleS }}-{{ $value->id }}"><i aria-hidden="true" class="fa fa-edit"></i> Edit</button>
                @include($moduleTitleP.'.edit')
                <button class="padding-bt btn btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ route('admin.'.$moduleTitleP.'.destroy',$value->id) }}"><i aria-hidden="true" class="fa fa-trash-o"></i> Delete</button>
            </td>
        </tr>
    @endforeach
@endif