<!-- Modal -->
<div class="modal fade" id="create-{{ $moduleTitleS }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create {{ ucwords($moduleTitleS) }}</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'admin.'.$moduleTitleP.'.store','method'=>'POST','autocomplete'=>'off')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Fullname:</strong>
                                {!! Form::text('fullname', Input::get('fullname'), array('placeholder' => 'Fullname','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Email:</strong>
                                {!! Form::text('email', Input::get('email'), array('placeholder' => 'Email','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Password:</strong>
                                {!! Form::password('password', array('placeholder' => 'Password','class'=>'form-control edit-password','AutoComplete'=>'off')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Confirm Password:</strong>
                                {!! Form::password('password_confirmation', array('placeholder' => 'Confirm Password','class'=>'form-control edit-password-c')) !!}
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud create-crud">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>