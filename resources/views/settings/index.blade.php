@extends($theme)

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Site Settings Management
            </h1>
        </div>
    </div>
</div>

{!! Form::open(array('route' => 'admin.settings.update','autocomplete'=>'off','files'=>'true')) !!}
<div class="grid-body no-border"> <br>
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-8">
            <div class="form-group {{ $errors->first($settings['site-title']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-title']['name'] !!}</label>
                <div class="controls">
                    {!! Form::text($settings['site-title']['slug'], $settings['site-title']['value'], array('placeholder' => 'Site URL','class' => 'form-control user-name-edit')) !!}
                    {!! $errors->first($settings['site-title']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['site-logo']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-logo']['name'] !!}</label>
                <div class="controls">
                    {!! Form::file($settings['site-logo']['slug'], null, array('class' => 'form-control')) !!}
                    {!! $errors->first($settings['site-logo']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['site-favicon']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-favicon']['name'] !!}</label>
                <div class="controls">
                    {!! Form::file($settings['site-favicon']['slug'], null, array('class' => 'form-control')) !!}
                    {!! $errors->first($settings['site-favicon']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->first($settings['site-keyword']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-keyword']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['site-keyword']['slug'], $settings['site-keyword']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['site-keyword']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['site-description']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-description']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['site-description']['slug'], $settings['site-description']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['site-description']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->first($settings['email']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['email']['name'] !!}</label>
                <div class="controls">
                    {!! Form::text($settings['email']['slug'], $settings['email']['value'], array('placeholder' => 'Site URL','class' => 'form-control user-name-edit')) !!}
                    {!! $errors->first($settings['email']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['footer-text']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['footer-text']['name'] !!}</label>
                <div class="controls">
                    {!! Form::text($settings['footer-text']['slug'], $settings['footer-text']['value'], array('placeholder' => 'Site URL','class' => 'form-control user-name-edit')) !!}
                    {!! $errors->first($settings['footer-text']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['aboutUs']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['aboutUs']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['aboutUs']['slug'], $settings['aboutUs']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit ckeditor','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['aboutUs']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['dmca']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['dmca']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['dmca']['slug'], $settings['dmca']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit ckeditor','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['dmca']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

        </div>
    </div>
    <button type="submit" value="general" class="btn btn-primary" name="general">Submit</button>
</div>
{!! Form::close() !!}
@endsection

@section('script')
<script src="{{ asset('/adminTheme/ckeditor/ckeditor.js') }}"></script>
@endsection