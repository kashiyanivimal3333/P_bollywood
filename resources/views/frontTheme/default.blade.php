<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
	<head>
		@yield('pageTitle')
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('/public/upload/settings/'.$settingsData['site-favicon']) }}" />
		<meta name="csrf-token" content="{!! csrf_token() !!}">
        <meta property="fb:app_id" content="360496400963522" />
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		{!! SEOMeta::generate() !!}
        {!! OpenGraph::generate() !!}
        {!! Twitter::generate() !!}

        <link href="{{ asset('/public/frontTheme/css/style.css') }}" rel="stylesheet" type="text/css"  media="all" />
		<link href="{{ asset('/public/frontTheme/css/mobile-custom.css') }}" rel="stylesheet" type="text/css"  media="all" />
		<link href='http://fonts.googleapis.com/css?family=Ropa+Sans' rel='stylesheet' type='text/css'>

	</head>
	<body>
	<!----start-wrap---->
	<div class="wrap">
		@include('frontTheme.header')
		@yield('content')
		
		<div class="clear"> </div>
	</div>
	<!----End-wrap---->
	
	@include('frontTheme.footer')

	<script src="{{ asset('/public/adminTheme/js/jquery.js') }}" type="text/javascript"></script>
	
	<script type="text/javascript">
		$(function() {
            var pull = $('#mobile-menu');
            menu = $('.top-nav');
            menuHeight = menu.height();

            $(pull).on('click', function() {
                menu.slideToggle("slow");
            });
        });
	</script>

	@yield('script')
	</body>
</html>

