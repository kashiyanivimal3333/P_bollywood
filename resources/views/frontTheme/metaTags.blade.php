<meta name="description" content="{{ $pageDescription }}" class="meta-dis">
<meta name="keywords" content="{{ $pageKeyword }}">

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{ $pageTitle }}">
<meta itemprop="description" content="{{ $pageDescription }}">
<meta itemprop="image" content="{{ URL::to('/') }}{{ $pageImage }}">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="{{ URL::to('/') }}">
<meta name="twitter:title" content="{{ $pageTitle }}">
<meta name="twitter:description" content="{{ $pageDescription }}">
<meta name="twitter:creator" content="{{ URL::to('/') }}">
<meta name="twitter:image" content="{{ URL::to('/') }}{{ $pageImage }}">

<!-- Open Graph data -->
<meta property="og:title" content="{{ $pageTitle }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ URL::to('/') }}" />
<meta property="og:image" content="{{ URL::to('/') }}{{ $pageImage }}" />
<meta property="og:description" content="{{ $pageDescription }}" />
<meta property="og:site_name" content="{{ URL::to('/') }}" />

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
