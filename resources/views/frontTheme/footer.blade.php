<div class="footer">
	<div class="wrap"> 
		<div class="box1">
			<div class="hide-box">
				<h4>Menu</h4>
				<ul>
					<li><a href="{{ route('front.home') }}">Home</a></li>
					<li><a href="{{ route('front.popular') }}">Popular</a></li>
					<li><a href="{{ route('front.aboutUs') }}">About</a></li>
					<li><a href="{{ route('front.contact') }}">Contact</a></li>
					<li><a href="{{ route('front.dmca') }}">DMCA</a></li>
				</ul>
			</div>
		</div>
		<div class="box1">
		<h4>Stay in touch on</h4>
				<ul class="social">
					<li><img src="/public/frontTheme/img/facebook.png" title="facebook"><a href="https://www.facebook.com/watchnewwrestling">Facebook</a></li>
					<li><img src="/public/frontTheme/img/twitter.png" title="twitter"><a href="#">Twitter</a></li>
					<li><img src="/public/frontTheme/img/gplus.png" title="google+"><a href="https://plus.google.com/106432724352066621414?hl=en">Google+</a></li>
				</ul>
		</div>
		<div class="clear"> </div>
	</div>
</div>

<div class="clear"> </div>

<div class="copy-right">
<p>&#169 {{ date('Y') }} {{ $settingsData['footer-text'] }}</p>
</div>