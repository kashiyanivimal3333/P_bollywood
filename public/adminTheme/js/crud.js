var optionsUser = { 
    complete: function(response) 
    {
    	console.log(response);
    	if($.isEmptyObject(response.responseJSON.error)){
    		window.location.href = current_page_url;
    	}else{
    		printErrorMsg(response.responseJSON.error);
    	}
    }
};

$("body").on("click",".create-crud",function(e){
	$(this).parents("form").ajaxForm(optionsUser);
});

function printErrorMsg (msg) {
	$(".print-error-msg").find("ul").html('');
	$(".print-error-msg").css('display','block');
	$.each( msg, function( key, value ) {
		$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
	});
}

$(".remove-crud").click(function(){
	var current_object = $(this);
	bootbox.confirm("Are you sure delete this item?", function(result) {
		if(result){	
		  	var action = current_object.attr('data-action');
		  	var token = $("input[name='_token']").val();
			var id = current_object.attr('data-id');
			$('body').html("<form class='form-inline remove-form' method='POST' action='"+action+"'></form>");
            $('body').find('.remove-form').append('<input name="_token" type="hidden" value="'+ token +'">');
			$('body').find('.remove-form').append('<input name="_method" type="hidden" value="DELETE">');
			$('body').find('.remove-form').append('<input name="id" type="hidden" value="'+ id +'">');
			$('body').find('.remove-form').submit();
		}
	}); 
});

$("body").on("click",".ban",function(){

      var current_object = $(this);

      bootbox.dialog({
      message: "<form class='form-inline add-to-ban' method='POST'><div class='form-group'><textarea class='form-control reason' rows='4' style='width:500px' placeholder='Add Reason for Ban this user.'></textarea></div></form>",
      title: "Add To Black List",
      buttons: {
        success: {
          label: "Submit",
          className: "btn-success",
          callback: function() {
                var baninfo = $('.reason').val();
                var token = $("input[name='_token']").val();
                var action = current_object.attr('data-action');
                var id = current_object.attr('data-id');

                if(baninfo == ''){
                    $('.reason').css('border-color','red');
                    return false;
                }else{
                    $('.add-to-ban').attr('action',action);
                    $('.add-to-ban').append('<input name="_token" type="hidden" value="'+ token +'">')
                    $('.add-to-ban').append('<input name="id" type="hidden" value="'+ id +'">')
                    $('.add-to-ban').append('<input name="baninfo" type="hidden" value="'+ baninfo +'">')
                    $('.add-to-ban').submit();
                }
                
          }
        },
        danger: {
          label: "Cancel",
          className: "btn-danger",
          callback: function() {
            // remove
          }
        },
      }
    });
});

/*=====  End of Add to Link Ajax  ======*/
    
/*======================================
=            Load Post Data            =
======================================*/
var postLoadPage = 1;
var flagPostLoad = 0;

$.loadPostData = function() {
    var token = $('meta[name=csrf-token]').attr("content");
    postLoadPage++;
    var form = $(".form-filter").serialize();
    form = form + '&page=' + postLoadPage;
    console.log(form);

    if(flagPostLoad == 0){
        $.ajax({
            url: current_page_url,
            method: 'GET',
            data: form,
            success: function(data) {
                if(data != ""){
                    $(".pagin-table").find("tbody").append(data);
                }else{
                    flagPostLoad = 1;
                }
            }
        });
    }
}

$(".search-crud").keyup(function(){
	var form = $(this).parents("form").serialize();
	flagPostLoad = 0;
	postLoadPage = 1;
	$.ajax({
        url: current_page_url,
        method: 'GET',
        data: form,
        success: function(data) {
            $(".pagin-table").find("tbody").html(data);
        }
    });
});

jQuery(".search-modules").click(function(e) {
    e.preventDefault();
    jQuery(".filter-panel").toggle("slow");
});