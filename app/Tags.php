<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Tags extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug' ,'name', 'meta_title', 'meta_keyword', 'meta_description'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    protected $guarded = array();

    public function getTags($input)
    {
        $data = static::select("tags.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy('id','desc')->paginate(10);
    }

    public function addTags($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function updateTags($id,$input)
    {
        return static::where('id',$id)->update(array_only($input,$this->fillable));
    }

    public function removeTags($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getTagsList()
    {
        return DB::table('tags')->lists('name','id');
    }

    public function getTotalTags()
    {
        return static::count();
    }

    public function getTagWithSlug($slug)
    {
        return static::where("slug",$slug)->first();
    }

}
