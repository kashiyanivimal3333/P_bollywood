<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Post extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','time', 'slug', 'image', 'description', 'meta_keyword', 'meta_description','name_title','name_keyword','name_description','domain_id','video_code'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    protected $guarded = array();

    public function getdata($value='')
    {
        return static::get();
    }

    public function getPosts($input)
    {
        $data = static::select("posts.*"
            ,DB::raw("GROUP_CONCAT(DISTINCT post_categories.category_id) as post_categories_list")
            ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.name) as post_categories_name")
            ,DB::raw("GROUP_CONCAT(DISTINCT post_tags.tag_id) as post_tags_list")
            )
            ->join("post_categories","post_categories.post_id","=","posts.id")
            ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
            ->leftjoin("post_tags","post_tags.post_id","=","posts.id");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        if($column == 'category_id'){
                            $data->where('post_categories.'.$column, $operator, "%{$row["value"]}%");
                        }else{
                            $data->where('posts.'.$column, $operator, "%{$row["value"]}%");
                        }
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy("id","desc")->groupBy("posts.id")->paginate(10);
    }

    public function AddPost($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function updatePost($id,$input)
    {
        return static::where('id',$id)->update(array_only($input,$this->fillable));
    }

    public function removePost($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getTotalPosts()
    {
        return static::count();
    }

    public function getLatestPost()
    {
        return static::select("posts.*"
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.name_display) as post_categories_name_display")
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.slug) as post_categories_slug")
                    )
                    ->join("post_categories","post_categories.post_id","=","posts.id")
                    ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
                    ->orderBy("posts.id","desc")
                    ->groupBy("posts.id")
                    ->paginate(9);
    }

    public function getPostDetail($slug)
    {
        return static::select("posts.*"
                    ,"domains.domain as domain"
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.name_display) as post_categories_name_display")
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.slug) as post_categories_slug")
                    ,DB::raw("GROUP_CONCAT(DISTINCT tags.name) as post_tag_name")
                    ,DB::raw("GROUP_CONCAT(DISTINCT tags.slug) as post_tag_slug")
                    )
                    ->join("post_categories","post_categories.post_id","=","posts.id")
                    ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
                    ->join("domains","domains.id","=","posts.domain_id")
                    ->leftjoin("post_tags","post_tags.post_id","=","posts.id")
                    ->leftjoin("tags","tags.id","=","post_tags.tag_id")
                    ->where("posts.slug",$slug)
                    ->groupBy("posts.id")
                    ->first();
    }

    public function getRelatedPost($postId)
    {
        $categoryData = static::select("posts.*"
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.id) as post_categories_list")
                    )
                    ->join("post_categories","post_categories.post_id","=","posts.id")
                    ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
                    ->where("posts.id",$postId)
                    ->groupBy("posts.id")
                    ->first();

        $categoryArray = explode(',', $categoryData->post_categories_list);
        return static::select("posts.*"
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.name_display) as post_categories_name_display")
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.slug) as post_categories_slug")        
                    )
                    ->join("post_categories","post_categories.post_id","=","posts.id")
                    ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
                    ->whereIn("sub_categories.id",$categoryArray)
                    ->groupBy("posts.id")
                    ->get()
                    ->take(6);
    }

    public function getPopularPost()
    {
        return static::select("posts.*",'sub_categories.name_display as categoryName','sub_categories.slug as categorySlug')
                    ->join('sub_categories','sub_categories.id','=','posts.category_id')
                    ->get()
                    ->take(1);
    }

    public function getRecentPost()
    {
        return static::orderBy("id","desc")->get()->take(3);
    }

    public function getPopularPostPage()
    {

        return static::select("posts.*"
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.name_display) as post_categories_name_display")
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.slug) as post_categories_slug")        
                    )
                    ->join("post_categories","post_categories.post_id","=","posts.id")
                    ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
                    ->orderBy("posts.view","desc")
                    ->groupBy("posts.id")
                    ->paginate(9);
    }
    public function getSubcatPopuler()
    {
        return static::select("posts.*"
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.name_display) as post_categories_name_display")
                    ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.slug) as post_categories_slug")        
                    )
                    ->join("post_categories","post_categories.post_id","=","posts.id")
                    ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
                    ->orderBy("posts.view","desc")
                    ->groupBy("posts.id")
                    ->get()
                    ->take(6);   
    }
    public function getCategoryPost($id)
    {
        return static::select("posts.*"
                        ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.name_display) as post_categories_name_display")
                        ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.slug) as post_categories_slug")
                    )
                    ->join("post_categories","post_categories.post_id","=","posts.id")
                    ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
                    ->where("post_categories.category_id","=",$id)
                    ->groupBy("posts.id")
                    ->orderBy("posts.id","desc")
                    ->paginate(9);
    }

    public function getTagPost($id)
    {
        return static::select("posts.*"
                        ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.name_display) as post_categories_name_display")
                        ,DB::raw("GROUP_CONCAT(DISTINCT sub_categories.slug) as post_categories_slug")
                    )
                    ->join("post_categories","post_categories.post_id","=","posts.id")
                    ->join("sub_categories","sub_categories.id","=","post_categories.category_id")
                    ->join("post_tags","post_tags.post_id","=","posts.id")
                    ->where("post_tags.tag_id","=",$id)
                    ->groupBy("posts.id")
                    ->orderBy("posts.id","desc")
                    ->paginate(9);
    }

    public function getSearchPost($text)
    {
        return static::where("title","like","%".$text."%")->get();
    }
}
