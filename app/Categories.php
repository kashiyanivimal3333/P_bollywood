<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Categories extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_display','name', 'slug', 'meta_keyword', 'meta_description'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $guarded = array();

    public function getCategories($input)
    {
        $data = static::select("categories.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy('id','desc')->paginate(10);
    }

    public function AddCategory($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function updateCategory($id,$input)
    {
        return static::where('id',$id)->update(array_only($input,$this->fillable));
    }

    public function removeCategory($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getCategoryList()
    {
        return DB::table('categories')->lists('name','id');
    }

    public function getTotalCategories()
    {
        return static::count();
    }

    public function getCatWithSlug($slug)
    {
        return static::where("slug",$slug)->first();
    }

    public function getcategoryListRight()
    {
        $data = static::select("categories.*"
                ,"sub_categories.id as subCategoriesId"
                ,"sub_categories.name as subCategoriesName"
                ,"sub_categories.slug as subCategoriesSlug"
                )
                ->leftjoin("sub_categories","sub_categories.category_id","=","categories.id")
                ->get();

        $result = [];
        if(!empty($data)){
            foreach ($data as $key => $value) {
               $result[$value->id]['data'] = $value;
               if(!empty($value->subCategoriesId)){
                    $result[$value->id]['subCategory'][$value->subCategoriesId] = $value;
               }
            }
        }

        // echo "<pre>";
        // print_r($result);
        // exit();

        return $result;

    }

}
