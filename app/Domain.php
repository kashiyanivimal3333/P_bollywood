<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Domain extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'domain'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'domains';

    protected $guarded = array();

    public function getDomains($input)
    {
        $data = static::select("domains.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy('id','desc')->paginate(10);
    }

    public function AddDomain($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function updateDomain($id,$input)
    {
        return static::where('id',$id)->update(array_only($input,$this->fillable));
    }

    public function removeDomain($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getDomainList()
    {
        return DB::table('domains')->lists('domain','id');
    }

    public function getTotalDomain()
    {
        return static::count();
    }

}
