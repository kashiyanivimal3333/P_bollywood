<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class SubCategories extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id','name_display','name', 'slug', 'meta_keyword', 'meta_description'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_categories';

    protected $guarded = array();

    public function getSubCategories($input)
    {
        $data = static::select("sub_categories.*"
                    ,"categories.name as categoryName"
                    )
                    ->join("categories","categories.id","=","sub_categories.category_id");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy('sub_categories.id','desc')->paginate(10);
    }

    public function AddSubCategories($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function updateSubCategories($id,$input)
    {
        return static::where('id',$id)->update(array_only($input,$this->fillable));
    }

    public function removeSubCategories($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getSubCategoriesList()
    {
        return DB::table('sub_categories')->lists('name','id');
    }

    public function getTotalSubCategories()
    {
        return static::count();
    }

    public function getSubCategoriesWithSlug($slug)
    {
        return static::where("slug",$slug)->first();
    }

}
