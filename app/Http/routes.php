<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', array('as'=> 'front.home', 'uses' => 'FrontHomeController@index'));

Route::get('aboutUs', array('as'=> 'front.aboutUs', 'uses' => 'FrontHomeController@aboutUs'));
Route::get('contact', array('as'=> 'front.contact', 'uses' => 'FrontHomeController@contact'));
Route::post('contact/store', array('as'=> 'front.feedback.create', 'uses' => 'FrontHomeController@createFeedback'));
Route::get('video/{slug}', array('as'=> 'front.post.detail', 'uses' => 'FrontHomeController@postDetail'));
Route::get('popular', array('as'=> 'front.popular', 'uses' => 'FrontHomeController@popularPost'));
Route::get('category/{slug}', array('as'=> 'front.category', 'uses' => 'FrontHomeController@getCategoryPost'));
Route::get('tag/{slug}', array('as'=> 'front.tag', 'uses' => 'FrontHomeController@getTagPost'));
Route::get('search', array('as'=> 'front.search', 'uses' => 'FrontHomeController@getSearchPost'));
Route::get('dmca', array('as'=> 'front.dmca', 'uses' => 'FrontHomeController@DMCA'));

Route::get('admin/login', array('as'=> 'admin.login', 'uses' => 'Auth\AdminAuthController@login'));
Route::post('admin/login', array('as'=> 'admin.login.post', 'uses' => 'Auth\AdminAuthController@loginPost'));

Route::group(['prefix' => 'admin'], function () {
	Route::get('home', array('as'=> 'admin.home', 'uses' => 'HomeController@index'));
	Route::get('logout', array('as'=> 'admin.logout', 'uses' => 'Auth\AdminAuthController@logout'));

	Route::get('userUserRevoke/{id}', array('as'=> 'admin.users.revokeuser', 'uses' => 'UserController@revoke'));
	Route::post('userBan', array('as'=> 'admin.users.ban', 'uses' => 'UserController@ban'));

	Route::resource('users', 'UserController');
	Route::resource('admins', 'AdminsController');
	Route::resource('categories', 'CategiriesController');
	Route::resource('sub-categories', 'SubCategiriesController');
	Route::resource('posts', 'PostsController');
	Route::resource('domains', 'DomainsController');
	Route::resource('tags', 'TagsController');
	Route::resource('names', 'NameController');

	Route::post('managePostTags/store', array('as'=> 'admin.postTags.store', 'uses' => 'PostsController@postTagsStore'));
	Route::post('manageNamesMeta/store', array('as'=> 'admin.namesMeta.store', 'uses' => 'PostsController@postNamesMetaStore'));
	
	// SettingsController
	Route::get('settings', array('as'=> 'admin.settings', 'uses' => 'SettingsController@index'));
	Route::post('settingsUpdate', array('as'=> 'admin.settings.update', 'uses' => 'SettingsController@store'));
	
	// FeedbackController
	Route::get('feedback', array('as'=> 'admin.feedback', 'uses' => 'FeedbackController@index'));
});
Route::get('{nameslug}/{postslug}', array('as'=> 'front.name', 'uses' => 'FrontHomeController@getNamePost'));

Route::get('sitemap', function(){
	ini_set('memory_limit', '1024M');
	set_time_limit(0); //60 seconds = 1 minute

    // create new sitemap object
    $sitemap = App::make("sitemap");

    $sitemap->add(URL::to('/'), '2012-08-25T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('/popular'), '2016-02-09T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('/aboutUs'), '2016-02-09T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('/contact'), '2016-02-09T20:10:00+02:00', '1.0', 'daily');

    $categorys = DB::table('sub_categories')->orderBy('created_at', 'desc')->get();

	// add every category to the sitemap
	foreach ($categorys as $category)
	{
	    $sitemap->add(URL::route('front.category',$category->slug), $category->updated_at, '1.0', 'daily');
	}

	$tags = DB::table('tags')->orderBy('created_at', 'desc')->get();

	// add every tag to the sitemap
	if(!empty($tags)){
		foreach ($tags as $tags)
		{
		    $sitemap->add(URL::route('front.tag',$tags->slug), $tags->updated_at, '1.0', 'daily');
		}
	}

	$posts = DB::table('posts')->orderBy('created_at', 'desc')->get();

    // add every post to the sitemap
    foreach ($posts as $post)
    {
     	$images = array();
         
        $images[] = array(
            'url' => URL::to('/')."/public/upload/posts/".$post->image,
            'title' => $post->title,
            'caption' => $post->title
        );
         
        $sitemap->add(URL::route('front.post.detail',$post->slug), $post->updated_at, '1.0', 'daily', $images);
    }

    return $sitemap->render('xml');
});

Route::get('sitemapname', function(){
	ini_set('memory_limit', '1024M');
	set_time_limit(0); //60 seconds = 1 minute

    // create new sitemap object
    $sitemap = App::make("sitemap");

    $names = DB::table('names')->get();
	$posts = DB::table('posts')->orderBy('created_at', 'desc')->get();

    // add every post to the sitemap
    foreach ($posts as $key=>$post)
    {
    	if(!empty($post->name_title) && !empty($post->name_keyword) && !empty($post->name_description)){
	        if(!empty($names)){
	        	$images = array();
	         
		        $images[] = array(
		            'url' => URL::to('/')."/public/upload/posts/".$post->image,
		            'title' => $post->title,
		            'caption' => $post->title
		        );

		        foreach ($names as $nkey => $nvalue) {
		        	$sitemap->add(URL::route('front.name',[$nvalue->slug,$post->slug]), $post->updated_at, '1.0', 'daily', $images);
		        }
	        }
    	}

    }

    return $sitemap->render('xml');
});