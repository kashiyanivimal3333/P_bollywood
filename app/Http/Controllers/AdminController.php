<?php namespace App\Http\Controllers;


class AdminController extends Controller
{
	
	public function __construct()
	{
		view()->share('theme','adminTheme.default');
		view()->share('login','adminTheme.login');
		view()->share('projectTitle','My Setup Project');
	}

	public function crudMessage($type, $module)
	{
		switch ($type) {
			case 'add':
				return $module . ' created successfully';
				break;
			case 'delete':
				return $module . ' deleted successfully';
				break;
			case 'update':
				return $module . ' updated successfully';
				break;
			
			default:
				# code...
				break;
		}
	}
}
