<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Post;
use App\Domain;
use App\Settings;
use App\Feedback;
use App\PostCategories;
use App\Tags;
use App\Names;
use App\SubCategories;
use Input;
use Validator;
use Redirect;
use DB;
use SEOMeta;
use OpenGraph;
use Twitter;
use SEO;
use URL;

class FrontHomeController extends FrontController
{
	
	public function __construct()
	{
		parent::__construct();
		$this->post = new Post;
		$this->categories = new Categories;
		$this->feedback = new Feedback;
		$this->postCategories = new PostCategories;
		$this->tags = new Tags;
		$this->names = new Names;
		$this->subCategories = new SubCategories;
	}

	public function SEOData()
    {
        $page = '';

        if(Input::has('page')){
            $page = 'Page '.Input::get('page').' - ';
        }

        $settings = Settings::get()->lists('value', 'slug');
        SEOMeta::setTitle($page.$settings['site-title']);
        SEOMeta::setDescription($page.$settings['site-description']);
        SEOMeta::addKeyword($settings['site-keyword']);

        OpenGraph::setDescription($page.$settings['site-description']);
        OpenGraph::setTitle($page.$settings['site-title']);
        OpenGraph::setUrl($this->siteURL);
        OpenGraph::addProperty('type', 'articles');

        Twitter::setTitle($settings['site-title']);
        Twitter::setSite($this->siteURL);

        ## Ou

        SEO::setTitle($page.$settings['site-title']);
        SEO::setDescription($page.$settings['site-description']);
        SEO::opengraph()->setUrl($this->siteURL);
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite($this->siteURL);
    }

	public function index()
	{
		$postData = $this->post->getLatestPost();

		$settingsData = Settings::get()->lists('value', 'slug');

		$pageTitle = $settingsData['site-title'];
		$this->SEOData();
		return view('front.home',compact('pageTitle','postData'));
	}

	public function aboutUs()
	{
		$settingsData = Settings::get()->lists('value', 'slug');
		$pageTitle = $settingsData['site-title'];
		$this->SEOData();
		return view('front.aboutUs',compact('pageTitle'));
	}

	public function contact()
	{
		$settingsData = Settings::get()->lists('value', 'slug');
		$pageTitle = $settingsData['site-title'];
		$this->SEOData();
		return view('front.contact',compact('pageTitle'));
	}

	public function postDetail($slug)
	{
		$postpopulerData = $this->post->getSubcatPopuler();
		$postData = $this->post->getPostDetail($slug);
		$relatedPostData = $this->post->getRelatedPost($postData->id);

		SEOMeta::setTitle($postData->title);
        SEOMeta::setDescription($postData->meta_description);
        SEOMeta::addKeyword($postData->meta_keyword);

        OpenGraph::setDescription($postData->meta_description);
        OpenGraph::setTitle($postData->title);
        OpenGraph::setUrl(URL::full());
        OpenGraph::addImage($this->siteURL.'/public/upload/posts/'.$postData->image);

        Twitter::setTitle($postData->title);
        Twitter::setSite(URL::full());

        ## Ou

        SEO::setTitle($postData->title);
        SEO::setDescription($postData->meta_description);
        SEO::opengraph()->setUrl(URL::full());
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite(URL::full());

        $pageTitle = $postData->title;
		DB::table('posts')->where('id',$postData->id)->increment('view', 1);
		return view('front.postDetail',compact('postpopulerData','postData','relatedPostData','pageTitle','pageKeyword','pageDescription','pageImage'));
	}

	public function popularPost()
	{

		$postData = $this->post->getPopularPostPage();
		$settingsData = Settings::get()->lists('value', 'slug');
		$pageTitle = $settingsData['site-title'];
		$this->SEOData();
		return view('front.popularPost',compact('postData','pageTitle','pageImage'));
	}

	public function getCategoryPost($slug)
	{
		$postpopulerData = $this->post->getSubcatPopuler();
		$categoryData = $this->subCategories->getSubCategoriesWithSlug($slug);
		
		$postData = $this->post->getCategoryPost($categoryData->id);

		$settingsData = Settings::get()->lists('value', 'slug');

		SEOMeta::setTitle($categoryData->name_display);
        SEOMeta::setDescription($categoryData->meta_description);
        SEOMeta::addKeyword($categoryData->meta_keyword);

        OpenGraph::setDescription($categoryData->meta_description);
        OpenGraph::setTitle($categoryData->name_display);
        OpenGraph::setUrl($this->siteURL);
        OpenGraph::addProperty('type', 'articles');

        Twitter::setTitle($categoryData->name_display);
        Twitter::setSite($this->siteURL);

        ## Ou

        SEO::setTitle($categoryData->name_display);
        SEO::setDescription($categoryData->meta_description);
        SEO::opengraph()->setUrl($this->siteURL);
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite($this->siteURL);
        
        $pageTitle = $categoryData->name_display;
		return view('front.categoryDetail',compact('postpopulerData','postData','categoryData','pageTitle','pageKeyword','pageDescription','pageImage'));
	}

	public function getTagPost($slug)
	{
		$tagData = $this->tags->getTagWithSlug($slug);
		$postData = $this->post->getTagPost($tagData->id);

		$settingsData = Settings::get()->lists('value', 'slug');
		$pageTitle = $tagData->meta_title;

		SEOMeta::setTitle($tagData->meta_title);
        SEOMeta::setDescription($tagData->meta_description);
        SEOMeta::addKeyword($tagData->meta_keyword);

        OpenGraph::setDescription($tagData->meta_description);
        OpenGraph::setTitle($tagData->meta_title);
        OpenGraph::setUrl($this->siteURL);
        OpenGraph::addProperty('type', 'articles');

        Twitter::setTitle($tagData->meta_title);
        Twitter::setSite($this->siteURL);

        ## Ou

        SEO::setTitle($tagData->meta_title);
        SEO::setDescription($tagData->meta_description);
        SEO::opengraph()->setUrl($this->siteURL);
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite($this->siteURL);
		
		return view('front.tagDetail',compact('postData','tagData','pageTitle','pageKeyword','pageDescription','pageImage'));
	}

	public function getSearchPost(Request $request)
	{
		$input = $request->all();

		$postData = $this->post->getSearchPost($input['text']);

		$settingsData = Settings::get()->lists('value', 'slug');
		$pageTitle = $settingsData['site-title'];
		$this->SEOData();
		return view('front.search',compact('postData','categoryData','pageTitle','pageKeyword','pageDescription','pageImage'));
	}

	public function createFeedback()
	{
		$input = Input::except(array('_token'));

         $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email|unique:feedback,email',
            'message' => 'required',
        ]);

        if ($validator->passes()) {
            $this->feedback->AddFeedback($input);
            return Redirect::back()
                	->with('success', 'Feedback Send Successfully.');
        }else{
        	return redirect('contact')
                        ->withErrors($validator)
                        ->withInput();
        }
	}

	public function DMCA()
	{
		$settingsData = Settings::get()->lists('value', 'slug');
		$pageTitle = 'DMCA';
		$pageKeyword = $settingsData['site-keyword'];
		$pageDescription = $settingsData['site-description'];
		$pageImage = '/public/upload/settings/'.$settingsData['site-logo'];
		return view('front.dmca',compact('pageTitle','pageKeyword','pageDescription','pageImage'));		
	}

	public function getNamePost(Request $request,$nameslug,$postslug)
	{
		
		$postData = $this->post->getPostDetail($postslug);
		$relatedPostData = $this->post->getRelatedPost($postData->id);

		$description = str_replace("$$$", $nameslug, $postData->name_description);
		$keyword = str_replace("$$$", $nameslug, $postData->name_keywords);
		$titleName = str_replace("$$$", $nameslug, $postData->name_title);

			SEOMeta::setTitle($titleName);
	        SEOMeta::setDescription($description);
	        SEOMeta::addKeyword($keyword);

	        OpenGraph::setDescription($description);
	        OpenGraph::setTitle($titleName);
	        OpenGraph::setUrl(URL::full());
	        OpenGraph::addProperty('type', 'articles');

	        Twitter::setTitle($titleName);
	        Twitter::setSite(URL::full());

	        ## Ou

	        SEO::setTitle($titleName);
	        SEO::setDescription($description);
	        SEO::opengraph()->setUrl(URL::full());
	        SEO::opengraph()->addProperty('type', 'articles');
	        SEO::twitter()->setSite(URL::full());

	        $pageTitle = $titleName;
			DB::table('posts')->where('id',$postData->id)->increment('view', 1);
			return view('front.postDetailName',compact('postData','relatedPostData','pageTitle','description','titleName'));
		
	}
}
