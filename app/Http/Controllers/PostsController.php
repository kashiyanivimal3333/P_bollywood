<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Admin;
use App\Categories;
use App\Post;
use App\PostCategories;
use App\ImageUpload;
use App\Tags;
use App\PostTags;
use App\SubCategories;
use App\Domain;
use Hash;
use Validator;

class PostsController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->admin = new Admin;
        $this->categories = new Categories;
        $this->post = new Post;
        $this->postCategories = new PostCategories;
        $this->tags = new Tags;
        $this->postTags = new PostTags;
        $this->subCategories = new SubCategories;
        $this->domain = new Domain;
        $this->moduleTitleS = 'post';
        $this->moduleTitleP = 'posts';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = $this->post->getPosts($request->all());

        $tagsList = $this->tags->getTagsList();
        $subCategoryList = $this->subCategories->getSubCategoriesList();
        $domainList = $this->domain->getDomainList();

        return view($this->moduleTitleP.'.index',compact('data','subCategoryList','tagsList','domainList'))
                    ->with('i', ($request->input('page', 1) - 1) * 5);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required',
            'category_id' => 'required',
            'title' => 'required|unique:posts',
            'time' => 'required',
            'domain_id' => 'required',
            'video_code' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {
            $input['slug'] = str_slug($input['title']);

            if($request->hasFile('image')){
                $input['image'] = ImageUpload::upload('/upload/posts/',$request->file('image'));
            }else{
                $input = array_except($input, ['image']);
            }

            $input = array_except($input, ['category_id']);

            $lastPostData = $this->post->AddPost($input);

            if($request->has('category_id')){
                foreach ($request->category_id as $key => $value) {
                    $cinput = [];
                    $cinput['post_id'] = $lastPostData->id;
                    $cinput['category_id'] = $value;
                    $this->postCategories->AddPostCategory($cinput);
                }
            }

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required|unique:posts,title,'.$id,
            'domain_id' => 'required',
            'time' => 'required',
            'video_code' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {
            $input['slug'] = str_slug($input['title']);

            if($request->hasFile('image')){
                $input['image'] = ImageUpload::upload('/upload/posts/',$request->file('image'));
            }else{
                $input = array_except($input, ['image']);
            }
            $input = array_except($input, ['category_id']);
            $this->post->updatePost($id,$input);

            if($request->has('category_id')){
                $this->postCategories->removePostCategory($id);
                foreach ($request->category_id as $key => $value) {
                    $cinput = [];
                    $cinput['post_id'] = $id;
                    $cinput['category_id'] = $value;
                    $this->postCategories->AddPostCategory($cinput);
                }
            }

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->post->removePost($id);
        $this->postCategories->removePostCategory($id);
        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route('admin.'.$this->moduleTitleP.'.index');
    }

    public function postTagsStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tags_id' => 'required',
            'post_id' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            if($request->has('tags_id')){
                $this->postTags->removeTags($input['post_id']);
                foreach ($request->tags_id as $key => $value) {
                    $tinput = [];
                    $tinput['post_id'] = $input['post_id'];
                    $tinput['tag_id'] = $value;
                    $this->postTags->addPostTags($tinput);
                }
            }

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);        
    }

    public function postNamesMetaStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_title' => 'required',
            'name_keyword' => 'required',
            'name_description' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            $this->post->updatePost($input['post_id'],$input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);        
    }
}
