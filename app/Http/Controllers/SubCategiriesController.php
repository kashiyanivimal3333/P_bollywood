<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Admin;
use App\Categories;
use App\SubCategories;
use Hash;
use Validator;

class SubCategiriesController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->admin = new Admin;
        $this->categories = new Categories;
        $this->subCategories = new SubCategories;

        $this->moduleTitleS = 'sub-category';
        $this->moduleTitleP = 'sub-categories';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->subCategories->getSubCategories($request->all());
        $categoriesList = $this->categories->getCategoryList();
        return view($this->moduleTitleP.'.index',compact('data','categoriesList'))
                    ->with('i', ($request->input('page', 1) - 1) * 5);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'name' => 'required',
            'name_display' => 'required',
        ]);
        $input = $request->all();
        if ($validator->passes()) {
            $input['slug'] = str_slug($input['name']);

            $this->subCategories->AddSubCategories($input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'name' => 'required',
            'name_display' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {
            $input['slug'] = str_slug($input['name']);

            $this->subCategories->updateSubCategories($id,$input);
            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->subCategories->removeSubCategories($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route('admin.'.$this->moduleTitleP.'.index');
    }
}