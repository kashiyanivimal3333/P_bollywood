<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Hash;
use Illuminate\Http\Request;

class AdminAuthController extends AdminController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $auth;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function login()
    {
        return view('auth.adminLogin');
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function loginPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if (auth()->guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
        {
            notificationMsg('success','you are login Successfully');
            return redirect()->route('admin.home');
        }else{
            notificationMsg('error','your username and password are wrong.');
            return redirect()->route('admin.login');
        }
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function logout()
    {
        auth()->logout();
        Session::flush();
        notificationMsg('success','you are logout Successfully');
        return redirect()->to('/');
    }
}
