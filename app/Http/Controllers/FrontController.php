<?php namespace App\Http\Controllers;

use App\Settings;
use App\Post;
use App\Categories;
use URL;

class FrontController extends Controller
{
	public $siteURL;

	public function __construct()
	{
		$this->siteURL = URL::to('/');
		$this->post = new Post;
		$this->categories = new Categories;

		view()->share('frontTheme','frontTheme.default');
		view()->share('frontThemeNew','frontThemeNew.default');

		$settingsData = Settings::get()->lists('value', 'slug');
		view()->share('settingsData',$settingsData);

		$categoryListRight = $this->categories->getcategoryListRight();
		view()->share('categoryListRight',$categoryListRight);
	}

}