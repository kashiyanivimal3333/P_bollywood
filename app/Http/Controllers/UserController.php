<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Hash;
use Validator;
use App\ImageUpload;

class UserController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->user = new User;
        $this->moduleTitleS = 'user';
        $this->moduleTitleP = 'users';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->user->getData($request->all());

        return view($this->moduleTitleP.'.index',compact('data'))
                    ->with('i', ($request->input('page', 1) - 1) * 5);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:password_confirmation',
            'fullname' => 'required',
            'imagepath' => 'required|image',
        ]);

        $input = $request->all();
        if ($validator->passes()) {
            $input['password'] = Hash::make($input['password']);
            $input = array_except($input,array('password_confirmation'));

            if($request->hasFile('imagepath')){
                $input['imagepath'] = ImageUpload::upload('/upload/users/',$request->file('imagepath'));
            }else{
                $input = array_except($input, ['imagepath']);
            }

            $this->user->AddData($input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:password_confirmation',
            'fullname' => 'required',
            'imagepath' => 'image',
        ]);

        $input = $request->all();
        if ($validator->passes()) {
            $input['password'] = Hash::make($input['password']);
            $input = array_except($input,array('password_confirmation'));

            if($request->hasFile('imagepath')){
                $input['imagepath'] = ImageUpload::upload('/upload/users/',$request->file('imagepath'));
            }else{
                $input = array_except($input, ['imagepath']);
            }

            $this->user->updateData($id,$input);

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user->destroyData($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route('admin.'.$this->moduleTitleP.'.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function ban(Request $request)
    {
        $input = $request->all();
        if(!empty($input['id'])){
            $this->user->userBan($input);
        }

        notificationMsg('success','User Ban Successfully.');
        return redirect()->route('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function revoke($id)
    {
        if(!empty($id)){
            $this->user->userRevoke($id);
        }

        notificationMsg('success','User Revoke Successfully.');
        return redirect()->route('admin.users.index');
    }
}
