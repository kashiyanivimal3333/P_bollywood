<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Admin;
use App\Domain;
use Hash;
use Validator;

class DomainsController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->admin = new Admin;
        $this->domain = new Domain;
        $this->moduleTitleS = 'domain';
        $this->moduleTitleP = 'domains';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->domain->getDomains($request->all());

        return view($this->moduleTitleP.'.index',compact('data'))
                    ->with('i', ($request->input('page', 1) - 1) * 5);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'domain' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            $this->domain->AddDomain($input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'domain' => 'required|unique:domains,domain,'.$id,
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            $this->domain->updateDomain($id,$input);
            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->domain->removeDomain($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route('admin.'.$this->moduleTitleP.'.index');
    }
}
