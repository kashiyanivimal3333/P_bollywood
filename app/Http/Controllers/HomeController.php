<?php namespace App\Http\Controllers;

use App\Categories;
use App\Post;
use App\Domain;

class HomeController extends AdminController
{
	
	public function __construct()
	{
		parent::__construct();
		$this->categories = new Categories;
		$this->post = new Post;
		$this->domain = new Domain;
	}

	public function index()
	{
		$totalCategory = $this->categories->getTotalCategories();
		$totalPost = $this->post->getTotalPosts();
		$totalDomain = $this->domain->getTotalDomain();
		return view('home.admin',compact('totalCategory','totalPost','totalDomain'));
	}

}
