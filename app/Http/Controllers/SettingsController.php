<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Settings;
use Hash;
use Validator;
use App\ImageUpload;

class SettingsController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->settings = new Settings;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $settings = $this->settings->getSettings();

        return view('settings.index',compact('settings'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validatorGeneral = Validator::make($input, [
            
        ]);

        if ($validatorGeneral->passes()) {

            if($request->hasFile('site-logo')){
                 $input['site-logo'] = ImageUpload::upload('upload/settings',$request->file('site-logo'));
            }else{
                $input = array_except($input,array('site-logo')); 
            }

            if($request->hasFile('site-favicon')){
                 $input['site-favicon'] = ImageUpload::upload('upload/settings',$request->file('site-favicon'));
            }else{
                $input = array_except($input,array('site-favicon')); 
            }

            $this->settings->updateSettings($input);

            return redirect()->route('admin.settings')
                ->with('success','Settings Successfully Update.');
        }
        
        return redirect()->route('admin.settings')
                ->withInput()
                ->with('errors',$validator->errors());
    }
}
