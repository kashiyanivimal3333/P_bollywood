<?php
	
	function notificationMsg($type, $message){
		\Session::put($type, $message);
	}

	function getUserImagePath($imagepath)
	{
		if(!empty($imagepath)){
			return "/upload/users/".$imagepath;
		}
		return "/adminTheme/img/avatar5.png";
	}

	function setPostTHImage1($image, $width, $height){
		if(!empty($image)){
			$imgext = strtolower(pathinfo($image, PATHINFO_EXTENSION));
			$supported_image = array('png','jpeg','jpg');

	    	if(in_array($imgext, $supported_image)){
				$real_path = 'public/upload/posts/'.$image;
				$new_image_path = 'public/upload/post_th_image1/'.$image;

				if(File::exists($real_path) === TRUE){
					if (File::exists($new_image_path) === FALSE) {
						Image::make($real_path)->resize($width, $height)->save($new_image_path);
						return '/public/upload/post_th_image1/'.$image;
					}else{
						return '/public/upload/post_th_image1/'.$image;
					}
					return '/public/frontTheme/img/default.jpg';
				}
			}	
			return '/public/frontTheme/img/default.jpg';
			
		}
		return '/public/frontTheme/img/default.jpg';
	}

	function setPostTHImage2($image, $width, $height){
		if(!empty($image)){
			$imgext = strtolower(pathinfo($image, PATHINFO_EXTENSION));
			$supported_image = array('png','jpeg','jpg');

	    	if(in_array($imgext, $supported_image)){
				$real_path = 'public/upload/posts/'.$image;
				$new_image_path = 'public/upload/post_th_image2/'.$image;

				if(File::exists($real_path) === TRUE){
					if (File::exists($new_image_path) === FALSE) {
						Image::make($real_path)->resize($width, $height)->save($new_image_path);
						return '/public/upload/post_th_image2/'.$image;
					}else{
						return '/public/upload/post_th_image2/'.$image;
					}
					return '/public/frontTheme/img/default.jpg';
				}
			}	
			return '/public/frontTheme/img/default.jpg';
			
		}
		return '/public/frontTheme/img/default.jpg';
	}

	function setPostTHImage3($image, $width, $height){
		if(!empty($image)){
			$imgext = strtolower(pathinfo($image, PATHINFO_EXTENSION));
			$supported_image = array('png','jpeg','jpg');

	    	if(in_array($imgext, $supported_image)){
				$real_path = 'upload/posts/'.$image;
				$new_image_path = 'upload/post_th_image3/'.$image;

				if(File::exists($real_path) === TRUE){
					if (File::exists($new_image_path) === FALSE) {
						Image::make($real_path)->resize($width, $height)->save($new_image_path);
						return '/upload/post_th_image3/'.$image;
					}else{
						return '/upload/post_th_image3/'.$image;
					}
					return '/frontTheme/img/default.jpg';
				}
			}	
			return '/frontTheme/img/default.jpg';
			
		}
		return '/frontTheme/img/default.jpg';
	}

	function setPostImageOriginal($image){
		if(!empty($image)){
			return '/public/upload/posts/'.$image;
		}
		return '/public/frontTheme/img/default.jpg';
	}