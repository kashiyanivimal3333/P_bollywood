<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class PostTags extends Model
{
    
    public $timestamps = false;   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id' ,'tag_id'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_tags';

    protected $guarded = array();


    public function addPostTags($input)
    {
        return static::create(array_only($input,$this->fillable));
    }
    
    public function removeTags($postId)
    {
        return static::where('post_id',$postId)->delete();
    }

}
