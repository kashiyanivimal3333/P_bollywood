<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class PostCategories extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id','category_id'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_categories';

    protected $guarded = array();

    public function AddPostCategory($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function removePostCategory($post_id)
    {
        return static::where('post_id',$post_id)->delete();
    }

}
