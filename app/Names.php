<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Names extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug' ,'name', 'meta_title', 'meta_keyword', 'meta_description'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'names';

    protected $guarded = array();

    public function getNames($input)
    {
        $data = static::select("names.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy('id','desc')->paginate(10);
    }

    public function addNames($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function updateNames($id,$input)
    {
        return static::where('id',$id)->update(array_only($input,$this->fillable));
    }

    public function removeNames($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getTagsList()
    {
        return DB::table('names')->lists('name','id');
    }

    public function getTotalTags()
    {
        return static::count();
    }

    public function getNameWithSlug($slug)
    {
        return static::where("slug",$slug)->first();
    }

}
